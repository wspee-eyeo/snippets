import json
import pprint
import pandas

from dateutil.parser import parse

def main(project_file_name):
    with open(project_file_name, 'r') as project_file:
        project = json.load(project_file)


    data = []
    for issue in project['issues']:
        labels = [label['label']['title'] for label in issue['label_links']]

        if not 'TeamJob' in labels:
            continue

        data.append({
            'id': issue['iid'],
            'title': issue['title'],
            'state': issue['state'],
            'labels': labels,
            'created_at': parse(issue['created_at']),
            'closed_at': parse(issue['closed_at']) if issue['closed_at'] else None,
        })


    df = pandas.DataFrame(data)
    df = df[df['state'] == 'closed']

    delta = df['closed_at'] - df['created_at']

    print(df, delta)

    print(len(df), delta.mean())

                
if __name__ == '__main__':
    main('project.json')
